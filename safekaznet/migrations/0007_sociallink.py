# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-26 15:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('safekaznet', '0006_qrcode'),
    ]

    operations = [
        migrations.CreateModel(
            name='SocialLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.CharField(blank=True, max_length=200, null=True)),
                ('image', models.ImageField(upload_to=b'social')),
            ],
            options={
                'verbose_name': 'Social link',
                'verbose_name_plural': 'Social links',
            },
        ),
    ]
