from modeltranslation.translator import translator, TranslationOptions
from safekaznet.models import SiteName

class SiteNameTranslationOptions(TranslationOptions):
    fields = ('name',)

translator.register(SiteName, SiteNameTranslationOptions)