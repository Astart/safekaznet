# encoding: utf-8
import os
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from pages.models import Page


PARTNER_CHOICES = (
    (1, 'Partner'),
    (2, 'Sponsor')
)

class SiteName(models.Model):
    name = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        verbose_name = _('Site name')

    def __unicode__(self):
        return unicode(self.name)


class Carousel(models.Model):
    title = models.CharField(max_length=150, blank=True, null=True)
    page = models.ForeignKey(Page, null=True, blank=True)
    image = models.ImageField(upload_to='carousel')
    published = models.BooleanField(default=False, null=False, blank=False)

    class Meta:
        verbose_name = _('Carousel')
        verbose_name_plural = _('Carousels')

    def __unicode__(self):
        return unicode(self.title)


class QRCode(models.Model):
    title = models.TextField(blank=True, null=True)
    image = models.ImageField(upload_to='')

    class Meta:
        verbose_name = _('QR Code')
        verbose_name_plural = _('QR Codes')


class SocialLink(models.Model):
    url = models.CharField(max_length=200, blank=True, null=True)
    image = models.ImageField(upload_to='social')

    class Meta:
        verbose_name = _('Social link')
        verbose_name_plural = _('Social links')

    def __unicode__(self):
        return unicode(self.url)


class Partner(models.Model):
    title = models.CharField(max_length=100, null=True, blank=True)
    url = models.CharField(max_length=200, blank=True, null=True)
    image = models.ImageField(upload_to='partner')
    type = models.PositiveSmallIntegerField(choices=PARTNER_CHOICES, null=True, blank=True)

    class Meta:
        verbose_name = _('Partner')
        verbose_name_plural = _('Partners')

    def __unicode__(self):
        return unicode(self.title)


class AdBlock(models.Model):
    title = models.CharField(max_length=50, null=True, blank=True)
    url = models.CharField(max_length=200, null=True, blank=True)
    image = models.ImageField(upload_to='ads')
    published = models.BooleanField(default=False, null=False, blank=False)

    class Meta:
        verbose_name = _('Ad block')
        verbose_name_plural = _('Ads block')

    def __unicode__(self):
        return unicode(self.title)