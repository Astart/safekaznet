from django.contrib import admin
from safekaznet.models import SiteName, Carousel, QRCode, SocialLink, Partner, AdBlock
from modeltranslation.admin import TranslationAdmin


class SiteNameAdmin(TranslationAdmin):
    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }

admin.site.register(SiteName, SiteNameAdmin)

admin.site.register(Carousel)
admin.site.register(QRCode)
admin.site.register(SocialLink)
admin.site.register(Partner)
admin.site.register(AdBlock)