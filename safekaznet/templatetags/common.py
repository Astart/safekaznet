import os
import re

from django import template
from django.utils.safestring import mark_safe
from django.conf import settings

from safekaznet.models import SiteName, Carousel, QRCode, SocialLink, Partner, AdBlock

register = template.Library()


@register.simple_tag()
def sitename():
    """
    return sitename tag
    """
    code = ''
    try:
        sitename = SiteName.objects.all()[:1].get()
        code = u'<div class="inline" id="site-name">%s</div>' % sitename.name
    except:
        pass
    return mark_safe(code)


@register.simple_tag()
def languages(current_language):
    code = ''
    for language in settings.LANGUAGES:
        if language[0] == current_language:
            pass
        else:
            code += u'<li><a href="">%s</a></li>' % language[1]
    return mark_safe(code)

@register.inclusion_tag('includes/carousel.html')
def carousel():
    slides = Carousel.objects.filter(published=True)

    return {'slides': slides}


@register.inclusion_tag('includes/qrcode.html')
def qrcode():
    qrcode = QRCode.objects.all()[:1].get()

    return {'qrcode': qrcode}


@register.inclusion_tag('includes/social.html')
def social():
    social_links = SocialLink.objects.all()

    return {'social_links': social_links}


@register.inclusion_tag('includes/partner.html')
def partner_slider(type):
    partners = Partner.objects.filter(type=type)

    return {'partners': partners}


@register.simple_tag()
def ad_block():
    ad = AdBlock.objects.filter(published=True).first()
    code = '<a href="%s"><img src="%s" alt="%s" /></a>' % (ad.url, ad.image.url, ad.title)
    return mark_safe(code)