--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO safekaznet;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO safekaznet;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO safekaznet;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO safekaznet;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO safekaznet;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO safekaznet;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO safekaznet;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO safekaznet;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO safekaznet;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO safekaznet;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO safekaznet;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO safekaznet;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO safekaznet;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO safekaznet;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO safekaznet;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO safekaznet;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO safekaznet;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO safekaznet;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO safekaznet;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO safekaznet;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO safekaznet;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: pages_content; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE pages_content (
    id integer NOT NULL,
    language character varying(5) NOT NULL,
    body text NOT NULL,
    type character varying(100) NOT NULL,
    creation_date timestamp with time zone NOT NULL,
    page_id integer NOT NULL
);


ALTER TABLE public.pages_content OWNER TO safekaznet;

--
-- Name: pages_content_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE pages_content_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pages_content_id_seq OWNER TO safekaznet;

--
-- Name: pages_content_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE pages_content_id_seq OWNED BY pages_content.id;


--
-- Name: pages_page; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE pages_page (
    id integer NOT NULL,
    creation_date timestamp with time zone NOT NULL,
    publication_date timestamp with time zone,
    publication_end_date timestamp with time zone,
    last_modification_date timestamp with time zone NOT NULL,
    status integer NOT NULL,
    template character varying(100),
    delegate_to character varying(100),
    freeze_date timestamp with time zone,
    redirect_to_url character varying(200),
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    author_id integer NOT NULL,
    parent_id integer,
    redirect_to_id integer,
    uuid uuid NOT NULL,
    CONSTRAINT pages_page_level_check CHECK ((level >= 0)),
    CONSTRAINT pages_page_lft_check CHECK ((lft >= 0)),
    CONSTRAINT pages_page_rght_check CHECK ((rght >= 0)),
    CONSTRAINT pages_page_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.pages_page OWNER TO safekaznet;

--
-- Name: pages_page_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE pages_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pages_page_id_seq OWNER TO safekaznet;

--
-- Name: pages_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE pages_page_id_seq OWNED BY pages_page.id;


--
-- Name: pages_pagealias; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE pages_pagealias (
    id integer NOT NULL,
    url character varying(255) NOT NULL,
    page_id integer
);


ALTER TABLE public.pages_pagealias OWNER TO safekaznet;

--
-- Name: pages_pagealias_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE pages_pagealias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pages_pagealias_id_seq OWNER TO safekaznet;

--
-- Name: pages_pagealias_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE pages_pagealias_id_seq OWNED BY pages_pagealias.id;


--
-- Name: safekaznet_carousel; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE safekaznet_carousel (
    id integer NOT NULL,
    title character varying(150),
    image character varying(100) NOT NULL,
    page_id integer,
    published boolean NOT NULL
);


ALTER TABLE public.safekaznet_carousel OWNER TO safekaznet;

--
-- Name: safekaznet_carousel_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE safekaznet_carousel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.safekaznet_carousel_id_seq OWNER TO safekaznet;

--
-- Name: safekaznet_carousel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE safekaznet_carousel_id_seq OWNED BY safekaznet_carousel.id;


--
-- Name: safekaznet_qrcode; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE safekaznet_qrcode (
    id integer NOT NULL,
    title text,
    image character varying(100) NOT NULL
);


ALTER TABLE public.safekaznet_qrcode OWNER TO safekaznet;

--
-- Name: safekaznet_qrcode_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE safekaznet_qrcode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.safekaznet_qrcode_id_seq OWNER TO safekaznet;

--
-- Name: safekaznet_qrcode_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE safekaznet_qrcode_id_seq OWNED BY safekaznet_qrcode.id;


--
-- Name: safekaznet_sitename; Type: TABLE; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE TABLE safekaznet_sitename (
    id integer NOT NULL,
    name character varying(400),
    name_en character varying(400),
    name_ru character varying(400),
    name_kk character varying(400)
);


ALTER TABLE public.safekaznet_sitename OWNER TO safekaznet;

--
-- Name: safekaznet_sitename_id_seq; Type: SEQUENCE; Schema: public; Owner: safekaznet
--

CREATE SEQUENCE safekaznet_sitename_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.safekaznet_sitename_id_seq OWNER TO safekaznet;

--
-- Name: safekaznet_sitename_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: safekaznet
--

ALTER SEQUENCE safekaznet_sitename_id_seq OWNED BY safekaznet_sitename.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY pages_content ALTER COLUMN id SET DEFAULT nextval('pages_content_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY pages_page ALTER COLUMN id SET DEFAULT nextval('pages_page_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY pages_pagealias ALTER COLUMN id SET DEFAULT nextval('pages_pagealias_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY safekaznet_carousel ALTER COLUMN id SET DEFAULT nextval('safekaznet_carousel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY safekaznet_qrcode ALTER COLUMN id SET DEFAULT nextval('safekaznet_qrcode_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY safekaznet_sitename ALTER COLUMN id SET DEFAULT nextval('safekaznet_sitename_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add site	7	add_site
20	Can change site	7	change_site
21	Can delete site	7	delete_site
22	Can add page	8	add_page
23	Can change page	8	change_page
24	Can delete page	8	delete_page
25	Can freeze page	8	can_freeze
26	Can publish page	8	can_publish
27	Manage Русский	8	can_manage_ru_RU
28	Manage English	8	can_manage_en_us
29	Can add content	9	add_content
30	Can change content	9	change_content
31	Can delete content	9	delete_content
32	Can add page alias	10	add_pagealias
33	Can change page alias	10	change_pagealias
34	Can delete page alias	10	delete_pagealias
35	Can add Site name	11	add_sitename
36	Can change Site name	11	change_sitename
37	Can delete Site name	11	delete_sitename
38	Can add Carousel	12	add_carousel
39	Can change Carousel	12	change_carousel
40	Can delete Carousel	12	delete_carousel
41	Manage Рус	8	can_manage_ru
42	Manage Eng	8	can_manage_en
43	Manage Қаз	8	can_manage_kz
44	Manage Қаз	8	can_manage_kk
45	Can add QR Code	13	add_qrcode
46	Can change QR Code	13	change_qrcode
47	Can delete QR Code	13	delete_qrcode
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('auth_permission_id_seq', 47, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$24000$19hAHDxIfujJ$DxBkNXSSs3xpF9xlm1ZZBchRKKcCU2XQ4CU+bKjaiLo=	2016-05-24 19:47:53.523216+06	t	admin			aidabult@gmail.com	t	t	2016-05-07 22:10:16.533976+06
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('auth_user_id_seq', 1, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2016-05-07 22:10:49.127843+06	1	Page 1	1	Added.	8	1
2	2016-05-11 23:09:58.548256+06	1	page-1	2	Changed publication_date and template.	8	1
3	2016-05-11 23:10:06.558083+06	1	Page 1	2	Changed publication_date and slug.	8	1
4	2016-05-11 23:10:21.948227+06	1	Page 1	2	Changed publication_date, title and slug.	8	1
5	2016-05-11 23:36:39.918491+06	1	Горячая линия по противодействию противоправному контенту в Казахстане	1	Added.	11	1
6	2016-05-15 23:17:44.802917+06	1	Carousel object	1	Added.	12	1
7	2016-05-15 23:24:42.060966+06	1	Carousel object	2	Changed published.	12	1
8	2016-05-15 23:53:41.572351+06	2	Carousel object	1	Added.	12	1
9	2016-05-17 21:10:57.79559+06	2	Carousel object	3		12	1
10	2016-05-17 21:10:57.87551+06	1	Carousel object	3		12	1
11	2016-05-17 21:11:14.034012+06	3	Carousel object	1	Added.	12	1
12	2016-05-17 21:21:20.47646+06	4	Carousel object	1	Added.	12	1
13	2016-05-17 21:21:32.441875+06	4	Carousel object	2	Changed published.	12	1
14	2016-05-24 20:43:32.91806+06	1	Page 1	2	Изменен publication_date, title и slug.	8	1
15	2016-05-24 20:43:45.230724+06	1	Page 1	2	Изменен publication_date, title и slug.	8	1
16	2016-05-24 20:44:02.803706+06	1	Page 1	2	Изменен publication_date, title и slug.	8	1
17	2016-05-24 20:44:13.876559+06	1	page-1	2	Изменен publication_date.	8	1
18	2016-05-24 20:44:35.667418+06	2	news	1	Добавлено.	8	1
19	2016-05-24 20:47:50.396517+06	3	duis-enim-lectus-habitasse-adipiscing-pulvinar-turpis-et	1	Добавлено.	8	1
20	2016-05-24 21:09:05.604979+06	4	cum-nunc-sit-purus-natoque-proin-nec-egestas-natoque-velit-monte	1	Добавлено.	8	1
21	2016-05-24 21:09:13.005331+06	4	cum-nunc-sit-purus-natoque-proin-nec-egestas-natoque-velit-monte	2	Изменен publication_date.	8	1
22	2016-05-24 21:09:22.898021+06	5	augue-augue-urna-pulvinar-dignissim-eros-nascetur-amet-elit-ut-u	1	Добавлено.	8	1
23	2016-05-24 21:09:28.70962+06	5	augue-augue-urna-pulvinar-dignissim-eros-nascetur-amet-elit-ut-u	2	Изменен publication_date и template.	8	1
24	2016-05-24 21:09:32.573364+06	5	augue-augue-urna-pulvinar-dignissim-eros-nascetur-amet-elit-ut-u	2	Изменен publication_date.	8	1
25	2016-05-24 21:09:47.300768+06	6	hac-odio-tempor-duis-lectus-lundium-ac-porta-pulvinar-adipiscing	1	Добавлено.	8	1
26	2016-05-24 21:17:07.229986+06	5	augue-augue-urna-pulvinar-dignissim-eros-nascetur-amet-elit-ut-u	2	Изменен publication_date и header_image.	8	1
27	2016-05-24 21:19:58.055168+06	5	augue-augue-urna-pulvinar-dignissim-eros-nascetur-amet-elit-ut-u	2	Изменен publication_date.	8	1
28	2016-05-24 21:21:48.605633+06	5	augue-augue-urna-pulvinar-dignissim-eros-nascetur-amet-elit-ut-u	2	Изменен publication_date.	8	1
29	2016-05-24 21:21:51.609572+06	5	augue-augue-urna-pulvinar-dignissim-eros-nascetur-amet-elit-ut-u	2	Изменен publication_date.	8	1
30	2016-05-24 21:22:14.122677+06	7	odio-sed-turpis-elementum-ut-mus-mus-montes-ac-tincidunt	1	Добавлено.	8	1
31	2016-05-24 21:22:21.332767+06	7	odio-sed-turpis-elementum-ut-mus-mus-montes-ac-tincidunt	2	Изменен publication_date и header_image.	8	1
32	2016-05-24 21:24:15.910649+06	7	odio-sed-turpis-elementum-ut-mus-mus-montes-ac-tincidunt	2	Изменен publication_date и headerimage.	8	1
33	2016-05-24 21:41:11.938719+06	3	duis-enim-lectus-habitasse-adipiscing-pulvinar-turpis-et	2	Изменен publication_date и headerimage.	8	1
34	2016-05-24 21:41:24.871386+06	4	cum-nunc-sit-purus-natoque-proin-nec-egestas-natoque-velit-monte	2	Изменен publication_date и headerimage.	8	1
35	2016-05-24 21:41:35.39365+06	6	hac-odio-tempor-duis-lectus-lundium-ac-porta-pulvinar-adipiscing	2	Изменен publication_date и headerimage.	8	1
36	2016-05-24 21:41:41.215832+06	5	augue-augue-urna-pulvinar-dignissim-eros-nascetur-amet-elit-ut-u	2	Изменен publication_date и headerimage.	8	1
37	2016-05-24 22:31:28.941553+06	7	odio-sed-turpis-elementum-ut-mus-mus-montes-ac-tincidunt	2	Изменен publication_date и teaser.	8	1
38	2016-05-24 22:35:18.161612+06	1	home	2	Изменен publication_date и slug.	8	1
39	2016-05-24 22:35:24.482144+06	1	home	2	Изменен publication_date и slug.	8	1
40	2016-05-24 22:35:29.974634+06	1	home	2	Изменен publication_date и slug.	8	1
41	2016-05-24 22:36:06.983554+06	2	page-2	2	Изменен publication_date и template.	8	1
42	2016-05-24 22:36:17.957759+06	2	news	2	Изменен publication_date и slug.	8	1
43	2016-05-24 22:36:23.979433+06	2	news	2	Изменен publication_date и slug.	8	1
44	2016-05-24 22:36:31.3234+06	2	news	2	Изменен publication_date.	8	1
45	2016-05-24 23:15:54.960482+06	4	Carousel object	2	Изменен image.	12	1
46	2016-05-24 23:16:41.700097+06	3	Carousel object	2	Изменен image.	12	1
47	2016-05-25 00:03:05.327419+06	8	articles	1	Добавлено.	8	1
48	2016-05-25 00:03:13.274741+06	8	articles	2	Изменен publication_date и slug.	8	1
49	2016-05-25 00:03:26.287028+06	8	articles	2	Изменен publication_date и slug.	8	1
50	2016-05-25 00:03:53.771182+06	9	hac-dictumst-penatibus-sagittis-eros-placerat-cum-mattis-auctor-	1	Добавлено.	8	1
51	2016-05-25 00:04:05.182175+06	10	hac-dictumst-penaatibus-sagittis-eros-placerat-cum-mattis-auctor-	1	Добавлено.	8	1
52	2016-05-25 00:04:11.321056+06	10	hac-dictumst-penaatibus-sagittis-eros-placerat-cum-mattis-auctor-	2	Изменен publication_date и teaser.	8	1
53	2016-05-25 00:04:19.850141+06	11	hac-dictumst-penatibus-sagittis-eros-placaaerat-cum-mattis-aucto	1	Добавлено.	8	1
54	2016-05-25 00:04:25.662351+06	11	hac-dictumst-penatibus-sagittis-eros-placaaerat-cum-mattis-aucto	2	Изменен publication_date и teaser.	8	1
55	2016-05-25 00:04:33.381653+06	12	hac-dictumst-penatibus-sagittis-eros-placerat-cum-mattis-auctor-	1	Добавлено.	8	1
56	2016-05-25 00:04:38.123013+06	12	hac-dictumst-penatibus-sagittis-eros-placerat-cum-mattis-auctor-	2	Изменен publication_date и teaser.	8	1
57	2016-05-25 00:04:45.320913+06	12	hac-dictumst-penatibus-sagittis-eros-placerat-cum-mattis-auctor-	2	Изменен publication_date и headerimage.	8	1
58	2016-05-25 00:04:54.864337+06	10	hac-dictumst-penaatibus-sagittis-eros-placerat-cum-mattis-auctor-	2	Изменен publication_date и headerimage.	8	1
59	2016-05-25 00:05:01.054545+06	11	hac-dictumst-penatibus-sagittis-eros-placaaerat-cum-mattis-aucto	2	Изменен publication_date и headerimage.	8	1
60	2016-05-25 00:05:11.295737+06	9	hac-dictumst-penatibus-sagittis-eros-placerat-cum-mattis-auctor-	2	Изменен publication_date и headerimage.	8	1
61	2016-05-25 00:56:20.842176+06	1	QRCode object	1	Добавлено.	13	1
62	2016-05-25 01:04:55.612093+06	1	QRCode object	2	Изменен title.	13	1
63	2016-05-25 01:05:42.332709+06	1	QRCode object	2	Изменен title.	13	1
64	2016-05-25 01:06:47.470361+06	1	QRCode object	2	Изменен title.	13	1
65	2016-05-25 01:07:05.3511+06	1	QRCode object	2	Изменен title.	13	1
66	2016-05-25 01:09:30.59491+06	1	QRCode object	2	Изменен title.	13	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 66, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	sites	site
8	pages	page
9	pages	content
10	pages	pagealias
11	safekaznet	sitename
12	safekaznet	carousel
13	safekaznet	qrcode
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('django_content_type_id_seq', 13, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2016-05-07 16:43:31.842579+06
2	auth	0001_initial	2016-05-07 16:43:32.608614+06
3	admin	0001_initial	2016-05-07 16:43:32.787211+06
4	admin	0002_logentry_remove_auto_add	2016-05-07 16:43:32.819252+06
5	contenttypes	0002_remove_content_type_name	2016-05-07 16:43:32.862895+06
6	auth	0002_alter_permission_name_max_length	2016-05-07 16:43:32.886018+06
7	auth	0003_alter_user_email_max_length	2016-05-07 16:43:32.962901+06
8	auth	0004_alter_user_username_opts	2016-05-07 16:43:32.985755+06
9	auth	0005_alter_user_last_login_null	2016-05-07 16:43:33.018268+06
10	auth	0006_require_contenttypes_0002	2016-05-07 16:43:33.029386+06
11	auth	0007_alter_validators_add_error_messages	2016-05-07 16:43:33.052227+06
12	sessions	0001_initial	2016-05-07 16:43:33.339719+06
13	sites	0001_initial	2016-05-07 17:48:16.745887+06
14	pages	0001_initial	2016-05-07 17:48:17.554607+06
15	pages	0002_page_sites	2016-05-07 17:48:17.755266+06
16	pages	0003_page_uuid	2016-05-07 17:48:18.174195+06
17	sites	0002_alter_domain_unique	2016-05-07 17:48:18.296464+06
18	safekaznet	0001_initial	2016-05-11 22:47:46.861271+06
19	pages	0004_auto_20160515_2316	2016-05-15 23:16:49.653734+06
20	safekaznet	0002_auto_20160515_2316	2016-05-15 23:16:50.440837+06
21	safekaznet	0003_carousel_published	2016-05-15 23:24:23.55245+06
22	pages	0005_auto_20160519_2215	2016-05-19 22:15:37.338696+06
23	safekaznet	0004_auto_20160519_2215	2016-05-19 22:15:37.772086+06
24	pages	0006_auto_20160519_2216	2016-05-19 22:16:51.060541+06
25	safekaznet	0005_auto_20160519_2216	2016-05-19 22:16:51.385832+06
26	safekaznet	0006_qrcode	2016-05-25 00:54:16.384137+06
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('django_migrations_id_seq', 26, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
b3jos882xyilwfw3rkn7rk75kmo7bfcn	YjM5MmUyNWZlNWFhZjhjMjAyNzllMzBmNmQ5OTBlMWNmYmM3ODFiZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjBmNmNiZDc2NGNiMzYyNDNlMTNjMmJhNTNlYjIwMWI0ZGRlZjJkOTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2016-05-21 22:10:27.850176+06
fhp97iqwb9neaqci3xm4o45oh1m7h6h0	YjM5MmUyNWZlNWFhZjhjMjAyNzllMzBmNmQ5OTBlMWNmYmM3ODFiZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjBmNmNiZDc2NGNiMzYyNDNlMTNjMmJhNTNlYjIwMWI0ZGRlZjJkOTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2016-06-07 19:47:53.545865+06
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Data for Name: pages_content; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY pages_content (id, language, body, type, creation_date, page_id) FROM stdin;
1	en-us	index	title	2016-05-07 22:10:49.102884+06	1
2	en-us		slug	2016-05-07 22:10:49.123422+06	1
3	en-us	page-1	slug	2016-05-11 23:09:58.485823+06	1
4	en-us		slug	2016-05-11 23:10:06.548017+06	1
5	ru-RU	Индекс	title	2016-05-11 23:10:21.936138+06	1
6	ru-RU		slug	2016-05-11 23:10:21.940667+06	1
7	ru	Index	title	2016-05-24 20:43:32.816792+06	1
8	ru		slug	2016-05-24 20:43:32.907791+06	1
9	ru	Индекс	title	2016-05-24 20:43:45.21878+06	1
10	en	Index	title	2016-05-24 20:44:02.791877+06	1
11	en		slug	2016-05-24 20:44:02.794876+06	1
12	kk	Индекс	title	2016-05-24 20:44:13.862021+06	1
13	kk	page-1	slug	2016-05-24 20:44:13.865643+06	1
14	kk	News	title	2016-05-24 20:44:35.652405+06	2
15	kk	news	slug	2016-05-24 20:44:35.654155+06	2
16	kk		teaser	2016-05-24 20:44:35.657733+06	2
17	kk		content	2016-05-24 20:44:35.659484+06	2
18	ru	A duis enim lectus in habitasse adipiscing pulvinar turpis et a.	title	2016-05-24 20:47:50.375778+06	3
19	ru	duis-enim-lectus-habitasse-adipiscing-pulvinar-turpis-et	slug	2016-05-24 20:47:50.378477+06	3
20	ru		teaser	2016-05-24 20:47:50.38412+06	3
21	ru		content	2016-05-24 20:47:50.386567+06	3
22	ru	Cum nunc sit purus natoque proin nec egestas natoque velit montes.	title	2016-05-24 21:09:05.581098+06	4
23	ru	cum-nunc-sit-purus-natoque-proin-nec-egestas-natoque-velit-monte	slug	2016-05-24 21:09:05.583707+06	4
24	ru		teaser	2016-05-24 21:09:05.589237+06	4
25	ru		content	2016-05-24 21:09:05.591776+06	4
26	ru	Augue augue urna pulvinar, dignissim eros nascetur amet. Elit ut ultricies.	title	2016-05-24 21:09:22.887035+06	5
27	ru	augue-augue-urna-pulvinar-dignissim-eros-nascetur-amet-elit-ut-u	slug	2016-05-24 21:09:22.889029+06	5
28	ru		teaser	2016-05-24 21:09:28.694665+06	5
29	ru		content	2016-05-24 21:09:28.698162+06	5
30	ru	Hac odio tempor duis lectus lundium ac porta pulvinar? Adipiscing. Sagittis.	title	2016-05-24 21:09:47.286047+06	6
31	ru	hac-odio-tempor-duis-lectus-lundium-ac-porta-pulvinar-adipiscing	slug	2016-05-24 21:09:47.287825+06	6
32	ru		teaser	2016-05-24 21:09:47.291668+06	6
33	ru		content	2016-05-24 21:09:47.293284+06	6
34	ru	upload/page_5/header_image-1464103027.21-internet_safety_for_kids.jpg	header_image	2016-05-24 21:17:07.213934+06	5
35	ru	Odio in sed turpis elementum ut mus mus montes ac, tincidunt.	title	2016-05-24 21:22:14.108426+06	7
36	ru	odio-sed-turpis-elementum-ut-mus-mus-montes-ac-tincidunt	slug	2016-05-24 21:22:14.110173+06	7
37	ru		teaser	2016-05-24 21:22:14.113664+06	7
38	ru		content	2016-05-24 21:22:14.115222+06	7
39	ru	upload/page_7/header_image-1464103341.3-internet_safety_for_kids.jpg	header_image	2016-05-24 21:22:21.323256+06	7
40	ru	upload/page_7/headerimage-1464103455.89-internet_safety_for_kids.jpg	headerimage	2016-05-24 21:24:15.893168+06	7
41	ru	upload/page_3/headerimage-1464104471.92-Screenshot from 2015-12-21 16:30:07.png	headerimage	2016-05-24 21:41:11.924004+06	3
42	ru	upload/page_4/headerimage-1464104484.85-kinopark.png	headerimage	2016-05-24 21:41:24.854914+06	4
43	ru	upload/page_6/headerimage-1464104495.37-kinopark.png	headerimage	2016-05-24 21:41:35.375395+06	6
44	ru	upload/page_5/headerimage-1464104501.2-Screenshot from 2015-12-21 16:30:07.png	headerimage	2016-05-24 21:41:41.200243+06	5
45	ru	<p>In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.</p>	teaser	2016-05-24 22:31:28.926272+06	7
46	ru	home	slug	2016-05-24 22:35:18.151453+06	1
47	en	home	slug	2016-05-24 22:35:24.471931+06	1
48	kk	home	slug	2016-05-24 22:35:29.963394+06	1
49	ru	News	title	2016-05-24 22:36:06.968449+06	2
50	ru	page-2	slug	2016-05-24 22:36:06.972365+06	2
51	ru	news	slug	2016-05-24 22:36:17.947747+06	2
52	en	News	title	2016-05-24 22:36:23.964591+06	2
53	en	news	slug	2016-05-24 22:36:23.967394+06	2
54	ru	articles	title	2016-05-25 00:03:05.316447+06	8
55	ru	articles	slug	2016-05-25 00:03:05.318172+06	8
56	en	articles	title	2016-05-25 00:03:13.261985+06	8
57	en	articles	slug	2016-05-25 00:03:13.265156+06	8
58	kk	articles	title	2016-05-25 00:03:26.273784+06	8
59	kk	articles	slug	2016-05-25 00:03:26.277043+06	8
60	ru	In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.	title	2016-05-25 00:03:53.753867+06	9
61	ru	hac-dictumst-penatibus-sagittis-eros-placerat-cum-mattis-auctor-	slug	2016-05-25 00:03:53.755962+06	9
62	ru	<p>In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.</p>	teaser	2016-05-25 00:03:53.760304+06	9
63	ru		content	2016-05-25 00:03:53.762124+06	9
64	ru	In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.	title	2016-05-25 00:04:05.168332+06	10
65	ru	hac-dictumst-penaatibus-sagittis-eros-placerat-cum-mattis-auctor-	slug	2016-05-25 00:04:05.169995+06	10
66	ru		teaser	2016-05-25 00:04:05.174046+06	10
67	ru		content	2016-05-25 00:04:05.176042+06	10
68	ru	<p>In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.</p>	teaser	2016-05-25 00:04:11.31009+06	10
69	ru	In hac dictumst? Penatibus sagittis! Eros, placaaerat cum mattis! Auctor urna.	title	2016-05-25 00:04:19.828842+06	11
70	ru	hac-dictumst-penatibus-sagittis-eros-placaaerat-cum-mattis-aucto	slug	2016-05-25 00:04:19.83077+06	11
71	ru		teaser	2016-05-25 00:04:19.835843+06	11
72	ru		content	2016-05-25 00:04:19.837722+06	11
73	ru	<p>In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.</p>	teaser	2016-05-25 00:04:25.65176+06	11
74	ru	In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.a	title	2016-05-25 00:04:33.367883+06	12
75	ru	hac-dictumst-penatibus-sagittis-eros-placerat-cum-mattis-auctor-	slug	2016-05-25 00:04:33.369505+06	12
76	ru		teaser	2016-05-25 00:04:33.373639+06	12
77	ru		content	2016-05-25 00:04:33.375508+06	12
78	ru	<p>In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.In hac dictumst? Penatibus sagittis! Eros, placerat cum mattis! Auctor urna.</p>	teaser	2016-05-25 00:04:38.111116+06	12
79	ru	upload/page_12/headerimage-1464113085.3-internet_safety_for_kids.jpg	headerimage	2016-05-25 00:04:45.305561+06	12
80	ru	upload/page_10/headerimage-1464113094.82-internet_safety_for_kids.jpg	headerimage	2016-05-25 00:04:54.849445+06	10
81	ru	upload/page_11/headerimage-1464113101.04-internet_safety_for_kids.jpg	headerimage	2016-05-25 00:05:01.045064+06	11
82	ru	upload/page_9/headerimage-1464113111.28-internet_safety_for_kids.jpg	headerimage	2016-05-25 00:05:11.280683+06	9
\.


--
-- Name: pages_content_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('pages_content_id_seq', 82, true);


--
-- Data for Name: pages_page; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY pages_page (id, creation_date, publication_date, publication_end_date, last_modification_date, status, template, delegate_to, freeze_date, redirect_to_url, lft, rght, tree_id, level, author_id, parent_id, redirect_to_id, uuid) FROM stdin;
3	2016-05-24 20:47:50.361327+06	2016-05-24 21:41:11.914453+06	\N	2016-05-24 21:41:11.914464+06	1	news.html		\N		10	11	2	1	1	2	\N	54f9c02b-9380-47cc-9d1c-b9665b4330dd
4	2016-05-24 21:09:05.560311+06	2016-05-24 21:41:24.84569+06	\N	2016-05-24 21:41:24.845701+06	1	news.html		\N		8	9	2	1	1	2	\N	3535eeed-d8cf-4843-8114-29f3a93d18e1
6	2016-05-24 21:09:47.278237+06	2016-05-24 21:41:35.365798+06	\N	2016-05-24 21:41:35.365807+06	1	news.html		\N		4	5	2	1	1	2	\N	b66d2f18-89ef-4883-b475-f6149762bab3
5	2016-05-24 21:09:22.877969+06	2016-05-24 21:41:41.189242+06	\N	2016-05-24 21:41:41.189252+06	1	news.html		\N		6	7	2	1	1	2	\N	d290b174-8afa-47f4-914c-675647470103
7	2016-05-24 21:22:14.102377+06	2016-05-24 22:31:28.917701+06	\N	2016-05-24 22:31:28.917711+06	1	news.html		\N		2	3	2	1	1	2	\N	15c63157-bbfd-4ede-a7aa-52f46bc2d828
1	2016-05-07 22:10:48.960321+06	2016-05-24 22:35:29.958617+06	\N	2016-05-24 22:35:29.958627+06	1	index.html		\N		1	2	1	0	1	\N	\N	bf10e372-f301-43f8-890a-648d6c10f9f8
2	2016-05-24 20:44:35.600901+06	2016-05-24 22:36:31.309647+06	\N	2016-05-24 22:36:31.309658+06	1	news_list.html		\N		1	12	2	0	1	\N	\N	dad105da-b2b6-4fdc-ada4-07fe795455a8
8	2016-05-25 00:03:05.310106+06	2016-05-25 00:03:26.270608+06	\N	2016-05-25 00:03:26.270619+06	1	articles_list.html		\N		1	10	3	0	1	\N	\N	5290078d-8b7d-4a6f-8181-de574ed542fe
9	2016-05-25 00:03:53.738683+06	2016-05-25 00:05:11.271495+06	\N	2016-05-25 00:05:11.271506+06	1	news.html		\N		8	9	3	1	1	8	\N	8de4bb77-efdb-4198-bf8f-6e55159b65d3
10	2016-05-25 00:04:05.161589+06	2016-05-25 00:04:54.816048+06	\N	2016-05-25 00:04:54.816058+06	1	news.html		\N		6	7	3	1	1	8	\N	4b441e31-4b5c-437c-a0e9-e24ecbedd6f1
11	2016-05-25 00:04:19.821689+06	2016-05-25 00:05:01.036847+06	\N	2016-05-25 00:05:01.036857+06	1	news.html		\N		4	5	3	1	1	8	\N	59f66ebf-ea44-4acc-83bf-c6ea6e34ee0e
12	2016-05-25 00:04:33.360658+06	2016-05-25 00:04:45.297439+06	\N	2016-05-25 00:04:45.297449+06	1	news.html		\N		2	3	3	1	1	8	\N	47132dc5-f7e7-4279-80e6-9c084e2ce698
\.


--
-- Name: pages_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('pages_page_id_seq', 12, true);


--
-- Data for Name: pages_pagealias; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY pages_pagealias (id, url, page_id) FROM stdin;
\.


--
-- Name: pages_pagealias_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('pages_pagealias_id_seq', 1, false);


--
-- Data for Name: safekaznet_carousel; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY safekaznet_carousel (id, title, image, page_id, published) FROM stdin;
4	test2	carousel/MNS_WideArea_Spotlight3.jpg	1	t
3	Test carousel	carousel/wide-angle-lens.jpg	1	t
\.


--
-- Name: safekaznet_carousel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('safekaznet_carousel_id_seq', 4, true);


--
-- Data for Name: safekaznet_qrcode; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY safekaznet_qrcode (id, title, image) FROM stdin;
1	&copy; 2016 <br>\r\nУполномоченный по правам <br>\r\nребенка Республики Казахстан. <br>\r\nВсе права защищены	./qr.png
\.


--
-- Name: safekaznet_qrcode_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('safekaznet_qrcode_id_seq', 1, true);


--
-- Data for Name: safekaznet_sitename; Type: TABLE DATA; Schema: public; Owner: safekaznet
--

COPY safekaznet_sitename (id, name, name_en, name_ru, name_kk) FROM stdin;
1	Горячая линия по противодействию противоправному контенту в Казахстане	Горячая линия по противодействию противоправному контенту в Казахстане	Горячая линия по противодействию противоправному контенту в Казахстане	\N
\.


--
-- Name: safekaznet_sitename_id_seq; Type: SEQUENCE SET; Schema: public; Owner: safekaznet
--

SELECT pg_catalog.setval('safekaznet_sitename_id_seq', 1, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: pages_content_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY pages_content
    ADD CONSTRAINT pages_content_pkey PRIMARY KEY (id);


--
-- Name: pages_page_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY pages_page
    ADD CONSTRAINT pages_page_pkey PRIMARY KEY (id);


--
-- Name: pages_pagealias_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY pages_pagealias
    ADD CONSTRAINT pages_pagealias_pkey PRIMARY KEY (id);


--
-- Name: pages_pagealias_url_key; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY pages_pagealias
    ADD CONSTRAINT pages_pagealias_url_key UNIQUE (url);


--
-- Name: safekaznet_carousel_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY safekaznet_carousel
    ADD CONSTRAINT safekaznet_carousel_pkey PRIMARY KEY (id);


--
-- Name: safekaznet_qrcode_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY safekaznet_qrcode
    ADD CONSTRAINT safekaznet_qrcode_pkey PRIMARY KEY (id);


--
-- Name: safekaznet_sitename_pkey; Type: CONSTRAINT; Schema: public; Owner: safekaznet; Tablespace: 
--

ALTER TABLE ONLY safekaznet_sitename
    ADD CONSTRAINT safekaznet_sitename_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX django_site_domain_a2e37b91_like ON django_site USING btree (domain varchar_pattern_ops);


--
-- Name: pages_content_1a63c800; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_content_1a63c800 ON pages_content USING btree (page_id);


--
-- Name: pages_content_599dcce2; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_content_599dcce2 ON pages_content USING btree (type);


--
-- Name: pages_content_type_fed4f50f_like; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_content_type_fed4f50f_like ON pages_content USING btree (type varchar_pattern_ops);


--
-- Name: pages_page_3cfbd988; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_page_3cfbd988 ON pages_page USING btree (rght);


--
-- Name: pages_page_4f331e2f; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_page_4f331e2f ON pages_page USING btree (author_id);


--
-- Name: pages_page_64e52e67; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_page_64e52e67 ON pages_page USING btree (redirect_to_id);


--
-- Name: pages_page_656442a0; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_page_656442a0 ON pages_page USING btree (tree_id);


--
-- Name: pages_page_6be37982; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_page_6be37982 ON pages_page USING btree (parent_id);


--
-- Name: pages_page_c9e9a848; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_page_c9e9a848 ON pages_page USING btree (level);


--
-- Name: pages_page_caf7cc51; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_page_caf7cc51 ON pages_page USING btree (lft);


--
-- Name: pages_pagealias_1a63c800; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_pagealias_1a63c800 ON pages_pagealias USING btree (page_id);


--
-- Name: pages_pagealias_url_eb4bd601_like; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX pages_pagealias_url_eb4bd601_like ON pages_pagealias USING btree (url varchar_pattern_ops);


--
-- Name: safekaznet_carousel_1a63c800; Type: INDEX; Schema: public; Owner: safekaznet; Tablespace: 
--

CREATE INDEX safekaznet_carousel_1a63c800 ON safekaznet_carousel USING btree (page_id);


--
-- Name: auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_content_type_id_c4bce8eb_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pages_content_page_id_bd3ae0c3_fk_pages_page_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY pages_content
    ADD CONSTRAINT pages_content_page_id_bd3ae0c3_fk_pages_page_id FOREIGN KEY (page_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pages_page_author_id_fed45c98_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY pages_page
    ADD CONSTRAINT pages_page_author_id_fed45c98_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pages_page_parent_id_133fa4d3_fk_pages_page_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY pages_page
    ADD CONSTRAINT pages_page_parent_id_133fa4d3_fk_pages_page_id FOREIGN KEY (parent_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pages_page_redirect_to_id_0e61cc5d_fk_pages_page_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY pages_page
    ADD CONSTRAINT pages_page_redirect_to_id_0e61cc5d_fk_pages_page_id FOREIGN KEY (redirect_to_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pages_pagealias_page_id_a7bed9b4_fk_pages_page_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY pages_pagealias
    ADD CONSTRAINT pages_pagealias_page_id_a7bed9b4_fk_pages_page_id FOREIGN KEY (page_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: safekaznet_carousel_page_id_61f7cc36_fk_pages_page_id; Type: FK CONSTRAINT; Schema: public; Owner: safekaznet
--

ALTER TABLE ONLY safekaznet_carousel
    ADD CONSTRAINT safekaznet_carousel_page_id_61f7cc36_fk_pages_page_id FOREIGN KEY (page_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

